<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>View Assets - Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

            <div class="col-lg-12 grid-margin stretch-card " id="dataDivCard" >
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Assets Table  </h4>
                        <p class="card-description">
                           <form class="form-sample">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Search with Asset Name , Asset Code , Asset Serial Number  </label>
                                        <div class="input-group ">
                                            <input type="text" class="form-control file-upload-info" id="searchInput"  placeholder="Search Table">
                                            <span class="input-group-append">
                                      <button class="file-upload-browse btn btn-info" onclick="onchangeData()" type="button">Search</button>
                                    </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect3">Group By</label>
                                            <select class="form-control form-control-sm" onchange="onchangeData()" id="groubySelection">
                                                <option value="all">All</option>
                                                <option value="asset_type">Asset Type</option>
                                                <option value="department">Departments</option>
                                                <option value="branch">Branch</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                        </p>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                       IS Auctioed
                                    </th>
                                    <th>
                                        Asset Name
                                    </th>
                                    <th>
                                        Department       (Branch)
                                    </th>
                                    <th>
                                        Asset Code
                                    </th>
                                    <th>
                                        Serial No.
                                    </th>
                                    <th>
                                       Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody >
                                <tr id="loadingRow">
                                    <td colspan="7">
                                        <center>
                                            <img src="images/spinner.gif" id="loadingSpinner">
                                        </center>

                                    </td>
                                </tr>
                                <!--<tr id=''>
                                    <td> May 15, 2015 </td>
                                </tr>-->

                                </tbody>
                                <tbody id="dataRowTable">
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

	        <?php
           require_once 'includes/editAsset.php' ;
           require_once 'includes/setAssetReapairs.php' ;
          ?>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>
  <script src="js/view-assets.js"></script>
  <script>
      loadAssetData('all' ,'');
  </script>

  <!-- End custom js for this page-->
</body>

</html>