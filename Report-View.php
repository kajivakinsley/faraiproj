<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Reports - Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
           <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <form class="forms-sample">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Select Reports</label> &nbsp;&nbsp;&nbsp;&nbsp; <img style="display: none;" id="loadingSpinner" src="images/spinner.gif">
                    <select class="form-control form-control-lg" onchange="reportSelectionChange()" id="reportsSelect">
                      <option value="null">Select Report</option>
                      <option value="assetsAll">Report on Overall Assets</option>
                      <option value="technicianAll">Report on Technicians</option>
                     
                    </select>
                    </div>
                     <div id="dataDataReport" class="form-group">
                        <div class="input-group">
                          
                          <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                      </div>
                  </form>
              </div>
            </div>
           </div>
          </div>



        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>

  <script src="js/highcharts/code/highcharts.js"></script>
  <script src="js/highcharts/code/modules/exporting.js"></script>
  <script src="js/highcharts/code/modules/export-data.js"></script>
  <script type="text/javascript">
                    var options = {
                    chart: {
                        renderTo: 'container',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Repairs Data Report'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b> Asset Repairs Count ' + this.point.name + '</b>: ' + this.y+ ' ';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>' + this.point.name + '</b>: ' + this.y;
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: []
                };
   function reportSelectionChange(){
     let reportsSelect = $("#reportsSelect").val();
      $("#loadingSpinner").slideDown('slow');
      if(reportsSelect === 'assetsAll'){

                $.getJSON("app/ajax/ajaxes/reports.php",{assetRepairs:'repaires'   },jsonresponse=>{
                  $("#loadingSpinner").slideUp('slow');
                $("#dataDataReport").slideDown('slow');
                 options.series = jsonresponse ;
                            chart = new Highcharts.Chart(options);
                });
      }else if(reportsSelect === 'technicianAll'){
        $.getJSON("app/ajax/ajaxes/reports.php",{               
                assetRepairsPerson:'repairesperson'
              },jsonresponse =>{
                 $("#loadingSpinner").slideUp('fast');
                  $("#dataDataReport").slideDown('slow');
                   options.series = jsonresponse ;
                              chart = new Highcharts.Chart(options);
              });
      }else if(reportsSelect === 'null'){
        $("#dataDataReport").slideUp('slow');
         $("#loadingSpinner").slideUp('fast');
      showErrorMessage("Select Report" , 4);
    }
   }
    

    </script>

  <!-- End custom js for this page-->
</body>

</html>