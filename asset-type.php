<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title> Asset Type- Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
 <div class="col-md-6 grid-margin stretch-card"  >
              <div class="card">
                <div class="card-body">
                  
                  <p class="card-description">
                   <h2>Add Asset Type</h2>
                  </p>
                  <form class="forms-sample" role="form" id="" onsubmit="return false;">
                    <div class="form-group">
                      <label for="exampleInputName1">Asset Type Name</label>
                      <input type="text" class="form-control" id="title" placeholder=".....">
                    </div>
                   
                    <div class="form-group">
                      <label for="exampleInputPassword4">Description</label>
                      <input type="text" class="form-control" id="description" placeholder=".....">
                    </div>
                   
                    
                     

                    <button onclick="saveDetails()" type="submit" class="btn btn-success mr-2">Save</button>
                    <button  type="reset" class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>
  <script type="text/javascript" src="js/for-pages/asset-type.js"></script>

  <!-- End custom js for this page-->
</body>

</html>