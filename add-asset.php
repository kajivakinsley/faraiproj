<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Add Asset - Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Assert Form</h4>
                        <p class="card-description">
                           add a new assert
                        </p>
                        <form class="forms-sample" onsubmit="return false;" role="form" id="assetForm">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                        require_once "app/DatabaseClass/DBAssetsTypes.php";
                                        require_once "app/DatabaseClass/DBDepartments.php";
	                                    $DBDepartmentsObject = new DBDepartments();

                                    ?>
                                    <div class="form-group">
                                        <label for="dpeartament">Department</label>
                                        <select class="form-control form-control-sm" id="dpeartament">
                                            <option value="null">Select</option>
                                            <?php
                                                $departmentsListData = $DBDepartmentsObject->getAllDepartments();
                                                while($departmentsRow = mysqli_fetch_assoc($departmentsListData)){
	                                                $departID = $departmentsRow['id'];
                                                    ?>
                                                    <option value="<?php print $departID ?>"><?php print $departmentsRow['title'] ?></option>

                                            <?php
                                                }

                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assetType">Asset Type</label>
                                        <select class="form-control form-control-sm" id="assetType">
                                            <option value="null">Select</option>
	                                        <?php
		                                        $DBAssetsTypesObject = new DBAssetsTypes();
		                                        $typesData = $DBAssetsTypesObject->getAllTypes();
		                                        while($typesRow = mysqli_fetch_assoc($typesData)){
			                                        $typID = $typesRow['id'];
			                                        ?>
                                                    <option value="<?php print $typID ?>"><?php print $typesRow['title'] ?></option>
		                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assetName">Name</label>
                                        <input type="text" class="form-control" id="assetName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assetCode">Code</label>
                                        <input type="text" class="form-control" id="assetCode" placeholder="Code">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assetSerialNo">Serial No.</label>
                                        <input type="text" class="form-control" id="assetSerialNo" placeholder="Serial Number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">  
                                        <label for="assetBranch">Branch</label>
                                        <select type="text" class="form-control" id="assetBranch" >
                                            <option value="null">Select</option>
                                            <option value="harare">Harare</option>
                                            <option value="bulawayo">Bulawayo</option>
                                            <option value="mutare">Mutare</option>
                                        </select>
                                    </div>
                                </div>
                               
                            </div>   
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">  
                                        <label for="assetSuppler">Supplier</label>
                                        <select type="text" class="form-control" id="assetSuppler" >
                                            <option value="null">Select</option>
                                            <?php 
                                            require_once "app/DatabaseClass/Suppliers.php";
                                            $suppObj = new Suppliers();

                                            $suppliersData = $suppObj->getSuppliers();
                                            while($supprw = mysqli_fetch_assoc($suppliersData)){

                                            ?>
                                            <option value="<?php print $supprw['id'] ;?>"><?php print $supprw['title'] ; ?></option>
                                        <?php } ?>

                                            
                                        </select>
                                    </div>
                                </div>
                               
                            </div>                         
                           


                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" rows="2"></textarea>
                            </div>
                            <button type="submit" id="btnSaveAsset" class="btn btn-success mr-2">Save</button>
                            <button type="reset" class="btn btn-light">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>
  <script src="js/for-pages/add-asset.js" ></script>

  <!-- End custom js for this page-->
</body>

</html>