<?php
	/**
	 * Created by PhpStorm.
	 * User: kajiva kinsley
	 * Date: 22/10/2018
	 * Time: 3:59 AM
	 */

	class Suppliers {
		private $DbCon;
		private $TABLE = "suppliers";


		public function __construct() {

			$this->DbCon = mysqli_connect("localhost" , "root" , "" ,"farkaj_inventory");

		}
		public function getSuppliers () {


			return  $this->qry("SELECT * FROM ".$this->TABLE);
			
		}
		public function getAllSuppliers ():array {


			$res =  $this->qry("SELECT * FROM ".$this->TABLE);
			$arr = array();
			while($row = mysqli_fetch_assoc($res)){
				$arr[] = $row ;
			}
			return $arr ;
		}
		public function saveSupplier (string $name , string $contact ,string $address):string{
			$contact = empty($contact) ? 'NULL' : "'$contact'";
			$address = empty($address) ? 'NULL' : "'$address'";

			return $this->qry("INSERT INTO ".$this->TABLE." (title , contact , address) VALUES ('$name', $contact , $address) ")?'saved' : 'failed';
		}

		private function  qry (string $queryString){

			return mysqli_query($this->DbCon , $queryString);
		}
	}