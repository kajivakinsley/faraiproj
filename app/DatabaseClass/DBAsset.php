<?php
	/**
	 * Created by PhpStorm.
	 * User: kajiva kinsley
	 * Date: 11/10/2018
	 * Time: 5:25 AM
	 */

	class DBAsset {
		private $DbCon;
		private $TABLE = "assets";


		public function __construct() {

			$this->DbCon = mysqli_connect("localhost" , "root" , "" ,"farkaj_inventory");

		}
		public function setForAuction($recId){
			return $this->qry("UPDATE  ".$this->TABLE." SET isauctioned = 1 WHERE id = $recId ")?'done':'failed';
		}
		public function updateAsset(int $recId , string $title , string $description ,string $asset_code ,string $serial_number ,
			int $asset_type  ,int $adepartment , $assetBranch /*, $assetSuppler*/  ):string{
			$description = empty($description) ? 'NULL' : "'$description'";
			$asset_code = empty($asset_code) ? 'NULL' : "'$asset_code'";
			$adepartment = empty($adepartment) ? 'NULL' : "'$adepartment'";
			$serial_number = empty($serial_number) ? 'NULL' : "'$serial_number'";

			return $this->qry("UPDATE  ".$this->TABLE." SET title = '$title', description = $description , asset_code =$asset_code ,department= $adepartment,
			 serial_number = $serial_number , asset_type = $asset_type , branch = '$assetBranch'  WHERE id = $recId  ")
				? 'saved' : 'failed';

		}
		public function saveAsset(string $title , string $description ,string $asset_code ,string $serial_number ,
			int $asset_type , int $saved_by ,int $stock_count ,int $adepartment ,string $assetBranch , int $supplier):string{
			$description = empty($description) ? 'NULL' : "'$description'";
			$asset_code = empty($asset_code) ? 'NULL' : "'$asset_code'";
			$adepartment = empty($adepartment) ? 'NULL' : "'$adepartment'";
			$serial_number = empty($serial_number) ? 'NULL' : "'$serial_number'";

			if(mysqli_num_rows($this->qry("SELECT serial_number , asset_code FROM ".$this->TABLE.
					" WHERE  serial_number = $serial_number OR asset_code = $asset_code  "))>0){
				return 'found';
			}

			return $this->qry("INSERT INTO ".$this->TABLE." (title , description , asset_code ,department ,
			 serial_number , asset_type , date_saved , saved_by , stock_count , branch , supplier ) ".
			"VALUES ('$title' , $description , $asset_code , $adepartment , $serial_number , $asset_type , NOW() , $saved_by , $stock_count , '$assetBranch' , $supplier ) ")
				? 'saved' : 'failed';

		}

		public function searchAsset($groupBy , $searchVal):string{
			$arr = array();
			if($groupBy == 'all' && empty($searchVal) ){
				$qry = $this->getAllAssets();
				while($row = mysqli_fetch_assoc($qry)){
					$arr[] =  $row;
				}
				return json_encode($arr);
			}
			$groupBy = $groupBy == 'all' ? '' : 'GROUP BY ' . $groupBy ;
			$sql = "SELECT assets.*, asset_types.title AS assetType, departments.title AS depart , COUNT(*) AS itemsCount
					FROM assets
					       JOIN asset_types ON asset_types.id = assets.asset_type
					       JOIN departments ON departments.id = assets.department
					WHERE assets.title LIKE '%$searchVal%'
					   OR assets.asset_code LIKE '%$searchVal%'
					   OR assets.branch LIKE '%$searchVal%'
					   OR assets.serial_number LIKE '%$searchVal%'
					   OR asset_types.title LIKE '%$searchVal%'
					   OR asset_types.title LIKE '%$searchVal%'
					        AND
					   (assets.isdeleted = 0 AND assets.isvisible = 1  ) " .$groupBy;
			$qry = $this->qry($sql);

			while($row = mysqli_fetch_assoc($qry)){
				$arr[] =  $row;
			}
			return json_encode($arr);
		}
		public function getStockCount(){
			return 
				$this->qry("SELECT assets.*  ,asset_types.title AS assetType ,
						       departments.title AS depart ,  COUNT(*) AS itemsCount
										FROM assets
						JOIN asset_types ON asset_types.id = assets.asset_type
						JOIN departments ON departments.id = assets.department
						                WHERE assets.isdeleted = 0 AND assets.isvisible = 1 AND  isauctioned = 0 GROUP BY assets.title   ")

			;
		}
		public function getAsset($recID){
			return mysqli_fetch_assoc(
				$this->qry("SELECT assets.*  ,asset_types.title AS assetType ,
						       departments.title AS depart
										FROM assets
						JOIN asset_types ON asset_types.id = assets.asset_type
						JOIN departments ON departments.id = assets.department
						                WHERE assets.isdeleted = 0 AND assets.isvisible = 1 AND  isauctioned = 0  AND  assets.id = $recID")

			);
		}
		public function getAllAssets(){
			return $this->qry("SELECT assets.*  ,asset_types.title AS assetType ,
						       departments.title AS depart ,
						       ((SELECT asset_repairs.status FROM asset_repairs WHERE asset_repairs.asset_id = assets.id ORDER by asset_repairs.date_saved DESC LIMIT 1 )) AS itemStatus
										FROM assets
						JOIN asset_types ON asset_types.id = assets.asset_type
						JOIN departments ON departments.id = assets.department
						                WHERE assets.isdeleted = 0 AND assets.isvisible = 1 AND  isauctioned = 0  ");
		}

		private function  qry (string $queryString){

			return mysqli_query($this->DbCon , $queryString);
		}

	}