<?php


class DBRepairs  {
		
		private $DbCon;
		private $TABLE = "asset_repairs";


		public function __construct() {

			$this->DbCon = mysqli_connect( "localhost" , "root" , "" ,"farkaj_inventory" );

		}

		public function saveRepairs(string $asset_id , string $about_repair , string $repair_by, string $sentBy , string $status 
		):string{
			
			return mysqli_query($this->DbCon , "INSERT INTO asset_repairs  ( asset_id  , about_repair , repair_by , sent_by , date_saved  , status , isvisible ) VALUES ('$asset_id' , '$about_repair' , '$repair_by' , '$sentBy'  , NOW() , '$status' , 1  )") ? 'saved':'failed';
		}


		public function updateRepair( string $status , string $repaires_final_description , string $recId ):string{

			return mysqli_query($this->DbCon , "UPDATE " . $this->TABLE . " SET repaires_final_description = '$repaires_final_description' , status = '$status' , date_saved = NOW()  WHERE id = $recId   ") ? 'done' : 'failed' ;
		}

		public function getAllRepairs(string $search){
						return mysqli_query($this->DbCon , empty($search) ?  "SELECT asset_repairs.* , assets.title AS assetName , assets.serial_number  , assets.asset_code , departments.title AS departmentt,
							      UCASE( users.username ) ,UCASE( (SELECT users.username FROM users WHERE users.id = asset_repairs.repair_by LIMIT 1) ) AS repairPerson
							FROM asset_repairs
							JOIN  assets ON assets.id = asset_repairs.asset_id
							JOIN users  ON users.id  = asset_repairs.sent_by
							JOIN departments  ON departments.id  = assets.department ORDER BY asset_repairs.date_saved DESC" :
				"SELECT asset_repairs.* , assets.title AS assetName , assets.serial_number ,  assets.asset_code  , departments.title AS departmentt,
				       users.username  , (SELECT users.username FROM users WHERE users.id = asset_repairs.repair_by LIMIT 1) AS repairPerson
				FROM asset_repairs
				JOIN  assets ON assets.id = asset_repairs.asset_id
				JOIN users  ON users.id  = asset_repairs.sent_by
				JOIN departments  ON departments.id  = assets.department
				WHERE asset_repairs.status LIKE '%$search%' OR assets.title LIKE '%$search%'  OR assets.serial_number LIKE '%$search%' OR departments.title LIKE '%$search%'
				   OR users.username LIKE '%$search%'  ORDER BY asset_repairs.date_saved DESC");
		}
		public function getAllRepairsReport(string $search){
						return mysqli_query($this->DbCon , empty($search) ?  "SELECT asset_repairs.* , assets.title AS assetName , assets.serial_number  , assets.asset_code , departments.title AS departmentt,
							      UCASE( users.username ) ,UCASE( (SELECT users.username FROM users WHERE users.id = asset_repairs.repair_by LIMIT 1) ) AS repairPerson , COUNT(*) AS itemsCount
							FROM asset_repairs
							JOIN  assets ON assets.id = asset_repairs.asset_id
							JOIN users  ON users.id  = asset_repairs.sent_by
							JOIN departments  ON departments.id  = assets.department  GROUP BY assets.title " :
				"SELECT asset_repairs.* , assets.title AS assetName , assets.serial_number ,  assets.asset_code  , departments.title AS departmentt,
				       users.username  , (SELECT users.username FROM users WHERE users.id = asset_repairs.repair_by LIMIT 1) AS repairPerson , COUNT(*) AS itemsCount
				FROM asset_repairs
				JOIN  assets ON assets.id = asset_repairs.asset_id
				JOIN users  ON users.id  = asset_repairs.sent_by
				JOIN departments  ON departments.id  = assets.department
				WHERE asset_repairs.status LIKE '%$search%' OR assets.title LIKE '%$search%'  OR assets.serial_number LIKE '%$search%' OR departments.title LIKE '%$search%'
				   OR users.username LIKE '%$search%' GROUP BY assets.title ");
		}
		
		public function getAllRepairsReportPerson(string $search){
						return mysqli_query($this->DbCon , empty($search) ?  "SELECT asset_repairs.* , assets.title AS assetName , assets.serial_number  , assets.asset_code , departments.title AS departmentt,
							      UCASE( users.username ) ,UCASE( (SELECT users.username FROM users WHERE users.id = asset_repairs.repair_by LIMIT 1) ) AS repairPerson , COUNT(*) AS itemsCount
							FROM asset_repairs
							JOIN  assets ON assets.id = asset_repairs.asset_id
							JOIN users  ON users.id  = asset_repairs.sent_by
							JOIN departments  ON departments.id  = assets.department  GROUP BY repairPerson " :
				"SELECT asset_repairs.* , assets.title AS assetName , assets.serial_number ,  assets.asset_code  , departments.title AS departmentt,
				       users.username  , (SELECT users.username FROM users WHERE users.id = asset_repairs.repair_by LIMIT 1) AS repairPerson , COUNT(*) AS itemsCount
				FROM asset_repairs
				JOIN  assets ON assets.id = asset_repairs.asset_id
				JOIN users  ON users.id  = asset_repairs.sent_by
				JOIN departments  ON departments.id  = assets.department
				WHERE asset_repairs.status LIKE '%$search%' OR assets.title LIKE '%$search%'  OR assets.serial_number LIKE '%$search%' OR departments.title LIKE '%$search%'
				   OR users.username LIKE '%$search%' GROUP BY repairPerson ");
		}



	}


?>