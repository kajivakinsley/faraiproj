<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Log In</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <form action="" method="post">
                <?php  
                $boderColor = "border-color: white;";

                $message = "";
                if(isset($_GET['err'])){
                  $message = "Log In Error";
                }
                if(isset($_POST['btnLogIn'])) {
                  $username = $_POST['username'];
                  $password = $_POST['password'];
                  $username = trim($username);
                  $password = trim($password);

                  if(empty($username)){
                     $message = "Put Usernanme";
                     $boderColor = "border-color: red;";
                  } else{
                      if(empty($password)){
                         $message = "Put Password";
                         $boderColor = "border-color: red;";
                    } else{
                      require_once "app/DatabaseClass/DBUsers.php";
                      $userObject = new DBUsers();
                      $userLog = $userObject->logIn($username , $password );
                      if($userLog =='failed'){
                        $message = "Failed to Log In . Please Use Correct Details" ;
                      }else if($userLog == 'found'){
                        if( $_SESSION['user_type'] == 'repairs'  ){
                          header("location:view-assets-to-repair.php");
                            exit;
                        }
                            header("location:add-asset.php");
                            exit;
                      }
                    }
                  }}
                  
                  

                    ?>
                <div class="form-group">
                   <label class="label" id="" style="color: red;"><?php echo $message; ?></label>
                </div>
                
                <div class="form-group">
                  <label class="label">Username</label>
                  <div class="input-group">
                    <input name="username"  style="<?php print $boderColor ;?>" type="text" class="form-control" placeholder="Username">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input style="<?php print $boderColor ;?>" name="password" type="password" class="form-control" placeholder="*********">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" name="btnLogIn" class="btn btn-primary submit-btn btn-block">Login</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                 
                  <a href="#" class="text-small forgot-password text-black">Forgot Password Contact Admin</a>
                </div>
               
               
              </form>
            </div>
           
           
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
</body>

</html>