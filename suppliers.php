<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Suppliers - Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
            <div class="col-lg-12 grid-margin stretch-card " id="dataDivCard" >
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Suppliers Table      <button class="btn btn-info" onclick="AddSupller()">Add</button></h4>


                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Suppliers Name
                                    </th>
                                    <th>
                                        Suppliers Contact
                                    </th>
                                    <th>
                                        Address
                                    </th>


                                </tr>
                                </thead>
                                <tbody >
                                <tr id="loadingRow">
                                    <td colspan="4">
                                        <center>
                                            <img src="images/spinner.gif" id="loadingSpinner">
                                        </center>

                                    </td>
                                </tr>
                                <!--<tr id=''>
                                    <td> May 15, 2015 </td>
                                </tr>-->

                                </tbody>
                                <tbody id="dataRowTable">
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;" id="supplerFormData" class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">SuppliersForm</h4>
                        <p class="card-description">
                            Add New Supplier
                        </p>
                        <form class="forms-sample" onsubmit="return false;" role="form" id="SuppliersForm">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="suppliersName">Name</label>
                                        <input type="text" class="form-control" id="suppliersName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="suppliersContact">Contact</label>
                                        <input type="text" class="form-control" id="suppliersContact" placeholder="Contact Number">
                                    </div>
                                </div>
                            </div>




                            <div class="form-group">
                                <label for="suppliersAddress">Address</label>
                                <textarea class="form-control" id="suppliersAddress" rows="2"></textarea>
                            </div>
                            <button type="submit" onclick="btnSuppliersAsset()" class="btn btn-success mr-2">Save</button>
                            <button type="reset"  class="btn btn-light">Cancel</button>
                            <button type="reset" onclick="closeSupplAdd()" class="btn btn-light">Close</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>
  <script src="js/for-pages/suppliers.js"></script>

  <!-- End custom js for this page-->
</body>

</html>