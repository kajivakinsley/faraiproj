<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title> View Repaires - Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          
          <div class="col-lg-12 grid-margin stretch-card " id="dataDivCard" >
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Assets Table  </h4>
                        <p class="card-description">
                           <form class="form-sample">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Search with Asset Name , Asset Code , Asset Serial Number  </label>
                                        <div class="input-group ">
                                            <input type="text" class="form-control file-upload-info" id="searchInput"  placeholder="Search Table">
                                            <span class="input-group-append">
                                      <button class="file-upload-browse btn btn-info" onclick="searchTableData();" type="button">Search</button>
                                    </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="visibility: hidden;">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect3">Group By</label>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                        </p>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Assaigned to
                                    </th>
                                    <th>
                                        Asset Name
                                    </th>
                                    <th>
                                        Department
                                    </th>
                                    <th>
                                        Asset Code
                                    </th>
                                    <th>
                                        Serial No.
                                    </th>
                                    <th>
                                       Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody >
                                <tr id="loadingRow">
                                    <td colspan="8">
                                        <center>
                                            <img src="images/spinner.gif" id="loadingSpinner">
                                        </center>

                                    </td>
                                </tr>
                                
                                <!--<tr id=''>
                                    <td> May 15, 2015 </td>
                                </tr>-->

                                </tbody>
                                <tbody id="dataRowTable">
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once 'includes/updateAssetReapairs.php' ;?>

        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>
  <script src="js/for-pages/view_repaires.js"></script>

  <!-- End custom js for this page-->
</body>

</html>