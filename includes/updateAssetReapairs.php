<div class="col-md-6 grid-margin stretch-card" id="updaterepaireditForm" style="display: none;">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">
				<button class="btn btn-info" onclick="backToDataTableAsstes()">
					Back
				</button>
				
			</h4>
			<p class="card-description">
				<h2>Update Asset Status</h2>
			</p>
			<span style="display: none; " id="repaireditId"></span>
			<form class="forms-sample" onsubmit="return false;" role="form" id="repairForm">
				<div class="row">
					<div class="col-md-6">
						<?php
							require_once "app/DatabaseClass/DBAssetsTypes.php";
							require_once "app/DatabaseClass/DBAsset.php";
							require_once "app/DatabaseClass/DBDepartments.php";
							require_once "app/DatabaseClass/DBUsers.php";
							$DBDepartmentsObject = new DBDepartments();
							$DBAsset = new DBAsset();
							$DBUsers = new DBUsers();

						?>
						<div class="form-group">
							<label for="repairperson">Repair Person</label>
							<select disabled class="form-control form-control-sm" id="repairperson">
								<option value="null">Select</option>
								<?php
									$repairesPersonsData = $DBUsers->getRepaisPersons();
									if(mysqli_num_rows($repairesPersonsData)){
									while($repairssRow = mysqli_fetch_assoc($repairesPersonsData) ){
										$useriD = $repairssRow['id'];
										?>
										<option value="<?php print $useriD ?>"><?php print $repairssRow['username'] ?></option>

										<?php
									}
								}

								?>

							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Asset </label>
							<select class="form-control form-control-sm" disabled id="repairAsset">
								<option value="null">Select</option>
								<?php
									
									$assetData = $DBAsset->getAllAssets();
									while($assetRow = mysqli_fetch_assoc($assetData)){
										$assetID = $assetRow['id'];
										?>
										<option value="<?php print $assetID ?>"><?php print $assetRow['title'] ?></option>
									<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="repairAssetstatus">Status</label>
							<select class="form-control form-control-sm" id="repairAssetstatus">
								<option value="null">Select</option>
								
								<option value="undone">Needs Attention</option>
								<option value="inprogress">still in progress</option>
								<option value="done">Repaired</option>

							</select>
						</div>
					</div>
				
				</div>
				



				<div class="form-group">
					<label for="repairdescription">Description</label>
					<textarea class="form-control" id="repairdescription" rows="2"></textarea>
				</div>
				<button type="submit" onclick="btnsaveAssetRepair()" class="btn btn-success mr-2">Save</button>
				<button type="reset" class="btn btn-light">Cancel</button>
			</form>
		</div>
	</div>
</div>