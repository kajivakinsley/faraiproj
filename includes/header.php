<?php 
          session_start();
              if(!isset($_SESSION)){
                header("location:index.php?err=log");
                exit;
              }
?>
<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
		<a  class="navbar-brand brand-logo" href="">
			<img src="images/logo.svg" alt="logo" />
		</a>
		<a class="navbar-brand brand-logo-mini" href="">
			<img src="images/logo-mini.svg" alt="logo" />
		</a>
	</div>

	<div class="navbar-menu-wrapper d-flex align-items-center">
		<!--<ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
			<li class="nav-item">
				<a href="#" class="nav-link">Schedule
					<span class="badge badge-primary ml-1">New</span>
				</a>
			</li>
			<li class="nav-item active">
				<a href="#" class="nav-link">
					<i class="mdi mdi-elevation-rise"></i>Reports</a>
			</li>
			<li class="nav-item">
				<a href="#" class="nav-link">
					<i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
			</li>
		</ul>-->
    <?php  if($_SESSION['user_type'] == 'admin' || $_SESSION['user_type'] === 'manager' ){ ?>
		<ul class="navbar-nav navbar-nav-right">
      
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="mdi mdi-file-document-box"></i>
              <?php 
              
              require_once "app/DatabaseClass/DBAsset.php" ;
              $DBAsset = new DBAsset();

              $dataDBAssets = $DBAsset->getStockCount();              
              $countAssets = mysqli_num_rows($dataDBAssets);
             
              ?>
              <span class="count"><?php echo  $countAssets ; ?> </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
            	<?php if( $countAssets > 0){?>
              <div class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left"> <?php echo $countAssets ;?> Stock Levels 
                </p>
               
              </div>
              <div class="dropdown-divider"></div>
              <?php while ($notificationRow = mysqli_fetch_assoc($dataDBAssets)){
              	if((int) $notificationRow['itemsCount']  < 6) {
              	?>

              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail" style="visibility: hidden;">
                 
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium text-dark"> <?php echo $notificationRow['title'];?>
                    <span class="float-right font-weight-light small-text">.</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    Stocks Quantity <?php echo $notificationRow['itemsCount'];?>
                  </p>
                </div>
              </a>
          <?php   }
           } 
         } 

          ?>
           
             
            </div>
          </li>
         
         
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Stocks Notfication </span>
              <img style="visibility: hidden;" class="img-xs rounded-circle" src="../../images/faces/face1.jpg" alt="Profile image">
            </a>
           
          </li>
        </ul>
         <?php } ?>
		<ul class="navbar-nav navbar-nav-right">

			
			<li class="nav-item dropdown d-none d-xl-inline-block">
				<a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
					<span class="profile-text"><?php echo ucfirst($_SESSION['username']) ; ?></span>
					<span id="userLogg" style="display:none;" ><?php echo $_SESSION['userID'];?></span>

					<img  style="visibility: hidden;" class="img-xs rounded-circle" src="images/faces/face1.jpg" alt="Profile image">
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">


					<a class="dropdown-item" href="logout.php">
						Log Out
					</a>
				</div>
			</li>
		</ul>
		<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
			<span class="mdi mdi-menu"></span>
		</button>
	</div>
</nav>