<div class="col-md-6 grid-margin stretch-card" id="editForm" style="display: none;">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">
				<button class="btn btn-info" onclick="backToDataTable()">
					Back
				</button>
			</h4>
			<p class="card-description">
				<h2>Edit Assert</h2>
			</p>
			<span style="display: none; " id="editId"></span>
			<form class="forms-sample" onsubmit="return false;" role="form" id="assetForm">
				<div class="row">
					<div class="col-md-6">
						<?php
							require_once "app/DatabaseClass/DBAssetsTypes.php";
							require_once "app/DatabaseClass/DBDepartments.php";
							$DBDepartmentsObject = new DBDepartments();

						?>
						<div class="form-group">
							<label for="dpeartament">Department</label>
							<select class="form-control form-control-sm" id="dpeartament">
								<option value="null">Select</option>
								<?php
									$departmentsListData = $DBDepartmentsObject->getAllDepartments();
									while($departmentsRow = mysqli_fetch_assoc($departmentsListData)){
										$departID = $departmentsRow['id'];
										?>
										<option value="<?php print $departID ?>"><?php print $departmentsRow['title'] ?></option>

										<?php
									}

								?>

							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="assetType">Asset Type</label>
							<select class="form-control form-control-sm" id="assetType">
								<option value="null">Select</option>
								<?php
									$DBAssetsTypesObject = new DBAssetsTypes();
									$typesData = $DBAssetsTypesObject->getAllTypes();
									while($typesRow = mysqli_fetch_assoc($typesData)){
										$typID = $typesRow['id'];
										?>
										<option value="<?php print $typID ?>"><?php print $typesRow['title'] ?></option>
									<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="assetName">Name</label>
							<input type="text" class="form-control" id="assetName" placeholder="Name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="assetCode">Code</label>
							<input type="text" class="form-control" id="assetCode" placeholder="Code">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="assetSerialNo">Serial No.</label>
							<input type="text" class="form-control" id="assetSerialNo" placeholder="Serial Number">
						</div>
					</div>
					 <div class="col-md-6">
                                    <div class="form-group">  
                                        <label for="assetBranch">Branch</label>
                                        <select type="text" class="form-control" id="assetBranch" >
                                            <option value="null">Select</option>
                                            <option value="harare">Harare</option>
                                            <option value="bulawayo">Bulawayo</option>
                                            <option value="mutare">Mutare</option>
                                        </select>
                                    </div>
                                </div>

				</div>




				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" id="description" rows="2"></textarea>
				</div>
				<button type="submit" onclick="btnUpdateAsset()" class="btn btn-success mr-2">Save</button>
				<button type="reset" class="btn btn-light">Cancel</button>
			</form>
		</div>
	</div>
</div>