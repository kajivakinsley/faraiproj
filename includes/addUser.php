 <div class="col-md-6 grid-margin stretch-card" id="addUserFormDiv" style="display: none;">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">
                    <button class="btn btn-info" onclick="backToDataTable()">Back</button>
                  </h4>
                  <p class="card-description">
                   <h2>User Info</h2>
                  </p>
                  <form class="forms-sample" role="form" id="addUserForm" onsubmit="return false;">
                    <div class="form-group">
                      <label for="exampleInputName1">User Name</label>
                      <input type="text" class="form-control" id="username" placeholder="Name">
                    </div>
                   
                    <div class="form-group">
                      <label for="exampleInputPassword4">Password</label>
                      <input type="password" class="form-control" id="password" placeholder="Password">
                    </div>
                   
                    <div class="form-group">
                      <label for="exampleInputCity1">User Type</label>
                      <select   class="form-control" id="userType">
                        <option value="null"> Select</option>
                        <option value="repairs"> Repairs</option>
                        <option value="admin"> Admin</option>
                        <option value="manager"> Manager</option>
                        </select>
                    </div>
                     

                    <button onclick="saveUserDetails()" type="submit" class="btn btn-success mr-2">Save</button>
                    <button  type="reset" class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>