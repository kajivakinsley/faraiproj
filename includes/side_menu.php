<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">
		<li class="nav-item nav-profile">
			<div class="nav-link">
				<div class="user-wrapper">

					<div style="visibility: hidden;"  class="text-wrapper">
						<p class="profile-name"><?php print $UserNamex =  $_SESSION['username'];?></p>
						<div>
							<span id="userUserID" style="display:none;"><?php print $UserIDx =  $_SESSION['userID'];?></span>

						</div>
					</div>
				</div>
			</div>
		</li>
		<li style="visibility: hidden;" class="nav-item">
			<a class="nav-link" href="">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">Dashboard</span>
			</a>
		</li>
		<?php 
		
		if( $_SESSION['user_type'] === 'admin' || $_SESSION['user_type'] === 'manager'  ){
			?>


		<li class="nav-item">
			<a class="nav-link" href="add-asset.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">Add Assets</span>
			</a>
		</li>
        <li class="nav-item">
			<a class="nav-link" href="view-assets.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">View Assets</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="asset-type.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">Add Asset Type</span>
			</a>
		</li>
		
        <li class="nav-item">
			<a class="nav-link" href="suppliers.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">Suppliers</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="view-assets-to-repair.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">View Repaires</span>
			</a>
		</li>
		 <li class="nav-item">
			<a class="nav-link" href="users.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">Users</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="Report-View.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">View Reports</span>
			</a>
		</li>
		<?php
		}else
		if( $_SESSION['user_type'] === 'repairs'  ){
		?>
		<li class="nav-item">
			<a class="nav-link" href="view-assets-to-repair.php">
				<i class="menu-icon mdi mdi-television"></i>
				<span class="menu-title">View Repaires</span>
			</a>
		</li>

	<?php } ?>


	</ul>
</nav>