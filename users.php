<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title> Users - Inventory System </title>
  <!-- plugins:css -->
    <?php require_once 'includes/shared_css.php' ;?>

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
	  <?php require_once 'includes/header.php' ;?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
	    <?php require_once 'includes/side_menu.php' ;?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

          <div class="col-lg-12 grid-margin stretch-card" id="usserTableDiv" style="">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Users </h4>
                  <p class="card-description">
                   <button onclick="addUser();" class="btn btn-info"> <i class="fa fa-user"> </i>  Add User
                   </button>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            User Name
                          </th>
                        
                          <th>
                            USer Type
                          </th>
                         
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <?php 
                      require_once 'app/DatabaseClass/DBUsers.php';
                      $userObj = new DBUsers ();
                      $userData =  $userObj ->getUsers();

                     
                       ?>
                     
                      <tbody>
                        <?php 
                         while( $row = mysqli_fetch_assoc($userData) ){
                           $iddd = $row['id'];
                        ?>
                        <tr id="<?php print $iddd ;?>">
                          
                          <td> <?php print ucfirst ($row['username'] );?> </td>
                         <td> <?php print ucfirst ($row['user_type'] ) ;?> </td>
                         <td> <button onclick="editUser(<?php print $iddd ; ?>);" class="btn btn-info">Edit User</button> </td>
                         
                        </tr>
                      <?php } ?>
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <?php 
            require_once "includes/addUser.php";
            require_once "includes/editUser.php";
            ?>


        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
	      <?php require_once 'includes/footer.php' ;?>

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <?php require_once 'includes/shared_js.php' ;?>
  <script src="js/for-pages/users.js"> </script>

  <!-- End custom js for this page-->
</body>

</html>