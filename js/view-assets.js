let user = $("#userLogg").text();


function btnUpdateAsset() {
    let dpeartament = $("#dpeartament").val().trim();
    let assetType = $("#assetType").val().trim();
    let assetName = $("#assetName").val().trim();
    let assetCode = $("#assetCode").val().trim();
    let assetSerialNo = $("#assetSerialNo").val().trim();
    let description = $("#description").val().trim();
    let assetBranch = $("#assetBranch").val().trim();


    if (dpeartament === 'null') {
        error_input_element(true, "dpeartament");
        showErrorMessage("Please Select a Department ", 3);
        return;
    } else {
        error_input_element(false, "dpeartament");
    }
    if (assetType === 'null') {
        error_input_element(true, "assetType");
        showErrorMessage("Please Select Asset Type", 3);
        return;
    } else {
        error_input_element(false, "assetType");
    }
    if (assetName === '') {
        error_input_element(true, "assetName");
        showErrorMessage("Set Name ", 3);
        return;
    } else {
        error_input_element(false, "assetName");
    }
    if (assetCode === '') {
        error_input_element(true, "assetCode");
        showErrorMessage("Set Asset Code ", 3);
        return;
    } else {
        error_input_element(false, "assetCode");
    }
    if (assetSerialNo === '') {
        error_input_element(true, "assetSerialNo");
        showErrorMessage("Set Serial Number", 3);
        return;
    } else {
        error_input_element(false, "assetSerialNo");
    }
    if (assetBranch === 'null') {
        error_input_element(true, "assetSerialNo");
        showErrorMessage("Set Branch", 3);
        return;
    } else {
        error_input_element(false, "assetBranch");
    }
    let recId = $("#editId").text();
    loadingScreen(true, "Updating ...");
    $.post("app/ajax/ajaxes/add_aset.php", {
        dpeartament: dpeartament,
        assetType: assetType,
        assetName: assetName,
        assetCode: assetCode,
        assetSerialNo: assetSerialNo,
        description: description,
        assetBranch:assetBranch ,
        idAsset: recId
    }, response => {
        loadingScreen(false, "Updating ...");
        if (response === 'saved') {
            $("#dpeartament").val("null");
            $("#assetType").val("null");
            $("#assetName").val("");
            $("#assetCode").val("");
            $("#assetSerialNo").val("");
            $("#description").val("");
            showSuccessMessage("Updated item", 4);
            backToDataTable();
            onchangeData();

        } else {
            showErrorMessage("Failed To Save", 4);
        }
    });
}

function creatRowView(dataRow) {
    let row = "";
    let groupBy = $("#groubySelection").val();
    if (!dataRow.length) {
        row += " <tr  id=''> " +
            "                <td> No Record Found</td>" +
            "                <td> No Record Found</td>" +
            "                <td> No Record Found</td>" +
            "                <td> No Record Found</td>" +
            "                <td> No Record Found</td>" +
            "                <td> No Record Found</td>" +
            "                <td> No Record Found</td>" +

            "    </tr>";
        return row;
    }
    for (let x = 0; x < dataRow.length; x++) {
        let data = dataRow[x];
        let isauctionedVAl = data.isauctioned == 1 ? "True" : "False";
        if(groupBy === 'branch'){
if (data.itemStatus === null || data.itemStatus === 'done') {
            
                 row += " <tr id='" + data.id + "'> " +
                "                <td>" + (x + 1) + " </td>" +
               "                <td> "+isauctionedVAl+" </td>" +
                "                <td>" + data.title.toUpperCase() + " </td>" +
                "                <td>" + data.depart + "  ("+ data.branch.toUpperCase()  + ") " + ' - '+ data.itemsCount +" </td>" +
                "                <td>" + data.asset_code + " </td>" +
                "                <td>" + data.serial_number.toUpperCase() + " </td>" +
                "                <td> <button disabled onclick='editAsset(" + data.id + ")' class='btn btn-info '>Edit</button> " +
                "                <button disabled onclick='setRepairsAsset(" + data.id + ")' class='btn btn-warning '>Set For Repairs </button> "+
                "                <button disabled onclick='assetAction("+data.id+")' class='btn btn-primary btn-sm'>Auction</button>" +
                "</td>" +

                "    </tr>";
         
           
        } else {
            row += " <tr id='" + data.id + "'> " +
                "                <td>" + (x + 1) + " </td>" +
                "                <td> "+isauctionedVAl+" </td>" +
                "                <td>" + data.title.toUpperCase() + " </td>" +
                "                <td>" + data.depart + "  ("+ data.branch.toUpperCase()  + ") " + ' - '+ data.itemsCount +" </td>" +
                "                <td>" + data.asset_code + " </td>" +
                "                <td >" + data.serial_number.toUpperCase() + " </td>" +
                "                <td> "+
               " <button disabled onclick='editAsset(" + data.id + ")' class='btn btn-info '>Edit</button> " +    

                "                <button disabled onclick='setRepairsAsset(" + data.id + ")' class='btn btn-warning '>Still at Repairs </button> "+
                 "               <button disabled onclick='assetAction("+data.id+")' class='btn btn-primary btn-sm'>Auction</button>" +
                " </td>" +

                "    </tr>";
        }
        }else{

        //console.log(data.itemStatus);

        if (data.itemStatus === null || data.itemStatus === 'done') {
            
                 row += " <tr id='" + data.id + "'> " +
                "                <td>" + (x + 1) + " </td>" +
               "                <td> "+isauctionedVAl+" </td>" +
                "                <td>" + data.title.toUpperCase() + " </td>" +
                "                <td>" + data.depart + "  ("+ data.branch.toUpperCase() + ") " + " </td>" +
                "                <td>" + data.asset_code + " </td>" +
                "                <td>" + data.serial_number.toUpperCase() + " </td>" +
                "                <td> <button onclick='editAsset(" + data.id + ")' class='btn btn-info '>Edit</button> " +
                "                <button onclick='setRepairsAsset(" + data.id + ")' class='btn btn-warning '>Set For Repairs </button> "+
                "                <button onclick='assetAction("+data.id+")' class='btn btn-primary btn-sm'>Auction</button>" +
                "</td>" +

                "    </tr>";
         
           
        } else {
            row += " <tr id='" + data.id + "'> " +
                "                <td>" + (x + 1) + " </td>" +
                "                <td> "+isauctionedVAl+" </td>" +
                "                <td>" + data.title.toUpperCase() + " </td>" +
                "                <td>" + data.depart +  "  ("+ data.branch.toUpperCase() + ") " + " </td>" +
                "                <td>" + data.asset_code + " </td>" +
                "                <td >" + data.serial_number.toUpperCase() + " </td>" +
                "                <td> "+
               " <button onclick='editAsset(" + data.id + ")' class='btn btn-info '>Edit</button> " +    

                "                <button disabled onclick='setRepairsAsset(" + data.id + ")' class='btn btn-warning '>Still at Repairs </button> "+
                 "               <button onclick='assetAction("+data.id+")' class='btn btn-primary btn-sm'>Auction</button>" +
                " </td>" +

                "    </tr>";
        }
       
        }


    }
    return row;
}
function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var checkbox in checkboxes)
    checkbox.checked = source.checked;
}
function assetAction(){
    let redID = arguments[ 0 ] ;
    loadingScreen(true , "posting to accounts");
    $.post("app/ajax/ajaxes/add_aset.php",{assetIDDAC:redID},
        response=>{
            loadingScreen(false , "posting to accounts");
            if(response === 'done'){
                showSuccessMessage("Asset Set for Auctioning" , 6);

            let roww = $("#" + redID);
            //console.log(roww.find('td:eq(5)').find('button:nth-child(2)'))
            roww.find('td:eq(1)').text('True');
            roww.find('td:eq(6)').find('button:nth-child(3)').prop('disabled', true)
            roww.find('td:eq(6)').find('button:nth-child(2)').prop('disabled', true)
            }else{
                showErrorMessage("Failed to post " ,4);
            }

        });
}
function backToDataTable() {
    $("#editForm").slideUp('fast', () => {
        $("#dataDivCard").slideDown('fast');
    });
}

function backToDataTableAsstes() {
    $("#repaireditForm").slideUp('fast', () => {
        $("#dataDivCard").slideDown('fast');
    });
}

function setRepairsAsset() {
    let assetId = arguments[0];
    $("#repaireditId").text(assetId);
    $("#repairAsset").val(assetId);
    $("#dataDivCard").slideUp('fast', () => {
        $("#repaireditForm").slideDown('fast');
    });
}

function editAsset() {
    let recID = arguments[0];
    $("#dataDivCard").slideUp('fast', () => {
        $("#editForm").slideDown('fast');
    });

    $("#editId").text(recID);

    loadingScreen(true, "Loading ...");
    $.getJSON("app/ajax/ajaxes/add_aset.php", {
        editIDAsset: recID
    }, response => {
        loadingScreen(false, "Loading ...");
        $("#dpeartament").val(response.department);
        $("#assetType").val(response.asset_type);
        $("#assetName").val(response.title);
        $("#assetCode").val(response.asset_code);
        $("#assetSerialNo").val(response.serial_number);
        $("#description").val(response.description);
        $("#assetBranch").val(response.branch);


    });
}

function onchangeData() {
    let groupBy = $("#groubySelection").val();
    let searchVal = $("#searchInput").val().trim();
    loadAssetData(groupBy, searchVal);

}

function loadAssetData(groupBy, searchVal) {
    let row = $("#loadingRow");
    row.slideDown('fast');

    $.getJSON("app/ajax/ajaxes/add_aset.php", {
        searchVal: searchVal,
        groupBy: groupBy
    }, response => {

        //  console.log(response)
        row.slideUp('fast');
        let rowHtml = creatRowView(response);
        $("#dataRowTable").html(rowHtml);

    });


}


function btnAssetRepair() {
    let repaireditId = $("#repaireditId").text();
    let repairperson = $("#repairperson").val();
    let repairAsset = $("#repairAsset").val();
    let repairAssetstatus = $("#repairAssetstatus").val();
    let repairdescription = $("#repairdescription").val();

    if (repairperson === 'null') {
        error_input_element(true, "repairperson");
        showErrorMessage("Select ", 3);
        return;
    } else {
        error_input_element(false, "repairperson");
    }
    if (repairAsset === 'null') {
        error_input_element(true, "repairAsset");
        showErrorMessage("Select ", 3);
        return;
    } else {
        error_input_element(false, "repairAsset");
    }
    if (repairAssetstatus === 'null') {
        error_input_element(true, "repairAssetstatus");
        showErrorMessage("Set Status ", 3);
        return;
    } else {
        error_input_element(false, "repairAssetstatus");
    }
    if (repairdescription === '') {
        error_input_element(true, "repairdescription");
        showErrorMessage("Set Description ", 3);
        return;
    } else {
        error_input_element(false, "repairdescription");
    }
    loadingScreen(true, "Saveing ...");
    $.post("app/ajax/ajaxes/add_aset.php", {
        user: user,
        asveAssetRepairs: 324,
        repairperson: repairperson,
        repairAsset: repairAsset,
        repairAssetstatus: repairAssetstatus,
        repairdescription: repairdescription


    }, resp => {

        loadingScreen(false, "Save ...");
        if (resp === 'saved') {
            $("#repairperson").val('null');
            $("#repairAsset").val('null');
            $("#repairAssetstatus").val('null');
            $("#repairdescription").val('');
            backToDataTableAsstes();
            showSuccessMessage("Saved", 3);


            let roww = $("#" + repaireditId);
            //console.log(roww.find('td:eq(5)').find('button:nth-child(2)'))
            roww.find('td:eq(6)').find('button:nth-child(2)').prop('disabled', true).text('Still at Repairs');


        } else {
            showErrorMessage("Failed to save ", 3);
        }

    });

}