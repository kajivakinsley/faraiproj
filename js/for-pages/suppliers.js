


function AddSupller () {
	$("#dataDivCard").slideUp('fast');
	$("#supplerFormData").slideDown('fast');

}
function closeSupplAdd () {
	$("#dataDivCard").slideDown('fast');
	$("#supplerFormData").slideUp('fast');
}
function getSuppliers () {
	let row = $("#loadingRow");

	row.slideDown('fast');
	$.getJSON("app/ajax/ajaxes/suppliers.php",{
		ssu:'aa'
	},response =>{
		let rows = "";
		if(response.length){
			for(let x = 0 ; x < response.length ; x ++){
				let data = response[x];

				rows += "<tr>"

				+ " <td>"+(x + 1 )+" </td>" +
				"  <td>"+data.title+" </td>" +
				"  <td>"+data.contact+" </td>" +
				"  <td>"+data.address+" </td>" +

				 + "</tr>";
			}
			row.slideUp('fast');

			$("#dataRowTable").html(rows);
		}
	});
}
getSuppliers();

function btnSuppliersAsset () {
	let  suppliersName = $("#suppliersName").val().trim();
	let  suppliersContact = $("#suppliersContact").val().trim();
	let  suppliersAddress = $("#suppliersAddress").val().trim();


	if (suppliersName === ''){
		error_input_element(true , "suppliersName");
		showErrorMessage("Set Name of Supplier" , 3);
		return;
	}else{
		error_input_element(false , "assetName");
	}

	loadingScreen(true , "Saving ...");
	$.post("app/ajax/ajaxes/suppliers.php" , {
		suppliersName:suppliersName,
		suppliersContact:suppliersContact,
		suppliersAddress:suppliersAddress
	} , response => {
		loadingScreen(false , "Saveing ...");
		if(response === 'saved'){
			showSuccessMessage("Saved Supplier" , 4);
			$("#suppliersName").val("");
			$("#suppliersContact").val("");
			$("#suppliersAddress").val("");
			getSuppliers ();
			$("#dataDivCard").slideDown('fast');
			$("#supplerFormData").slideUp('fast');
		}else{
			showErrorMessage("Failed To Save" , 4);
		}
	});

}













