function searchTableData() {
    let serch = $("#searchInput").val().trim();
    getTableData(serch);

}

function getTableData(search) {

    let dataRowTable = $("#dataRowTable");
    let loadingRow = $("#loadingRow");
    loadingRow.slideDown('fast');
    dataRowTable.slideUp('fast');

    $.getJSON("app/ajax/ajaxes/repaires.php", {
        search: search
    }, response => {

        //console.log(response);
        loadingRow.slideUp('fast');
        dataRowTable.slideDown('fast');
        if (response.length) {

            dataRowTable.html(
                getRows(response)
            );

        }

    });
}

getTableData('');

function getRows(data) {

    let row = "";

    for (let x = 0; x < data.length; x++) {
        let daRow = data[x];
        let id = daRow.id;
        if (daRow.status == 'done') {


            row += "<tr id='" + id + "'>" +
                "<td> " + daRow.status + " </td>" +
                "<td> " + daRow.date_saved.split(' ')[0] + " </td>" +
                "<td> " + daRow.repairPerson + " </td>" +
                "<td> " + daRow.assetName + " </td>" +
                "<td> " + daRow.departmentt + " </td>" +
                "<td> " + daRow.asset_code + " </td>" +
                "<td> " + daRow.serial_number + " </td>" +

                "<td>" +
                "  <button disabled onclick='updateItem(" + id + " , " + daRow.repair_by + " , " + daRow.sent_by + " , " + daRow.asset_id + "  )' class='btn btn-info'> Update Item </button>" +
                " </td>" +
                " </tr>";
        } else {
            row += "<tr id='" + id + "'>" +
                "<td> " + daRow.status + " </td>" +
                "<td> " + daRow.date_saved.split(' ')[0] + " </td>" +
                "<td> " + daRow.repairPerson + " </td>" +
                "<td> " + daRow.assetName + " </td>" +
                "<td> " + daRow.departmentt + " </td>" +
                "<td> " + daRow.asset_code + " </td>" +
                "<td> " + daRow.serial_number + " </td>" +

                "<td>" +
                "  <button onclick='updateItem(" + id + " , " + daRow.repair_by + " , " + daRow.sent_by + " , " + daRow.asset_id + "  )' class='btn btn-info'> Update Item </button>" +
                " </td>" +
                " </tr>";
        }
    }

    return row;
}

let rowID;
let repairby;
let sentBy;
let asset_id;
let asset_status;

function updateItem() {
    rowID = arguments[0];
    repairby = arguments[1];
    sentBy = arguments[2];
    asset_id = arguments[3];

    $("#repairperson").val(repairby);
    $("#repairAsset").val(asset_id);


    $("#dataDivCard").slideUp('slow', () => {
        $("#updaterepaireditForm").slideDown('slow');
    });
}

function backToDataTableAsstes() {
    $("#updaterepaireditForm").slideUp('slow', () => {
        $("#dataDivCard").slideDown('slow');
    });
}

function btnsaveAssetRepair() {

    let repairAssetstatus = $("#repairAssetstatus").val();
    let repairdescription = $("#repairdescription").val();

    if (repairAssetstatus === 'null') {
        error_input_element(true, "repairAssetstatus");
        showErrorMessage("Select ", 3);
        return;
    } else {
        error_input_element(false, "repairAssetstatus");
    }

    if (repairAssetstatus === 'done' && repairdescription === '') {
        showErrorMessage("Make a final description ", 3);
        return;
    }
    loadingScreen(true, "Saving ...");
    $.post("app/ajax/ajaxes/repaires.php", {
        updaterowID: rowID,
        updaterepairAssetstatus: repairAssetstatus,
        updaterepairdescription: repairdescription
    }, response => {
        loadingScreen(false, "Saving ...");
        if (response === 'done') {
            $("#repairAssetstatus").val('null');
            $("#repairdescription").val('');
            showSuccessMessage("Saved ", 3);
            $("#updaterepaireditForm").slideUp('fast', () => {
                $("#dataDivCard").slideDown('fast');
            });
            let rww = $("#" + rowID);
            rww.find('td:eq(0)').text(repairAssetstatus);
            if (repairAssetstatus == 'done') {

                rww.find('td:eq(7)').find('button:nth-child(1)').prop('disabled', true);
            } else {

            }

        } else {
            showErrorMessage("Failed to update", 4);
        }
    });


}