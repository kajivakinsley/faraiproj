function editUser() {
    lastEditId = arguments[0];

    $("#usserTableDiv").slideUp('slow', () => {
        $("#editUserFormDiv").slideDown('slow');
    });

    $.getJSON("app/ajax/ajaxes/users.php", {
        idd: lastEditId
    }, resp => {
        $("#edittusername").val(resp.username);
        $("#editpassword").val(resp.password);
        $("#edituserType").val(resp.user_type);
    });



}
function editTablebackToDataTable(){
    $("#editUserFormDiv").slideUp('slow', () => {
        $("#usserTableDiv").slideDown('slow');
    });
}
let lastEditId;


function addUser() {
    $("#usserTableDiv").slideUp('slow', () => {
        $("#addUserFormDiv").slideDown('slow');
    });

}

function saveUserDetails() {
    let username = $("#username").val();
    let password = $("#password").val();
    let userType = $("#userType").val();

    if (username === '') {
        error_input_element(true, "username");
        showErrorMessage("Put Username", 3);
        return;
    } else {
        error_input_element(false, "username");
    }
    if (password === '') {
        error_input_element(true, "password");
        showErrorMessage("Put Password", 3);
        return;
    } else {
        error_input_element(false, "password");
    }
    if (userType === 'null') {
        error_input_element(true, "userType");
        showErrorMessage(" Select User Type", 3);
        return;
    } else {
        error_input_element(false, "userType");
    }
    loadingScreen(true, "Saveing ...");
    $.post('app/ajax/ajaxes/users.php', {
        df: 23,
        username: username,
        password: password,
        userType: userType
    }, (data, textStatus, xhr) => {
        /*optional stuff to do after success */
        loadingScreen(false, "");
        if (data === 'saved') {
            showSuccessMessage("Saved User", 3);
            $("#username").val('');
            $("#password").val('');
            $("#userType").val('null');

        } else {
            showErrorMessage("Failed to save", 3);
        }

    });



}

function editUserDetails() {
    let username = $("#edittusername").val();
    let password = $("#editpassword").val();
    let userType = $("#edituserType").val();

    if (username === '') {
        error_input_element(true, "edittusername");
        showErrorMessage("Put Username", 3);
        return;
    } else {
        error_input_element(false, "edittusername");
    }
    if (password === '') {
        error_input_element(true, "editpassword");
        showErrorMessage("Put Password", 3);
        return;
    } else {
        error_input_element(false, "editpassword");
    }
    if (userType === 'null') {
        error_input_element(true, "edituserType");
        showErrorMessage(" Select User Type", 3);
        return;
    } else {
        error_input_element(false, "edituserType");
    }
    loadingScreen(true, "Saveing ...");
    $.post('app/ajax/ajaxes/users.php', {
        eddf: lastEditId,
        username: username,
        password: password,
        userType: userType
    }, (data, textStatus, xhr) => {
        /*optional stuff to do after success */
        loadingScreen(false, "");
        if (data === 'saved') {
            showSuccessMessage("Updated User", 3);
            $("#edittusername").val('');
            $("#editpassword").val('');
            $("#edituserType").val('null');
            editTablebackToDataTable();
            location.reload();
            let roww = $("#" + lastEditId);
            roww.find('td:eq(0)').tex(username);

            roww.find('td:eq(1)').tex(userType);



        } else {
            showErrorMessage("Failed to update", 3);
        }

    });



}

function backToDataTable() {
    $("#addUserFormDiv").slideUp('slow', () => {
        $("#usserTableDiv").slideDown('slow');
    });
}