function saveDetails() {
    let title = $("#title").val().trim();
    let description = $("#description").val().trim();

    if (title === '') {
        error_input_element(true, "title");
        showErrorMessage("Please put Title of asset ", 6);
        return;
    } else {
        error_input_element(false, "title");
    }
    if (description === '') {
        error_input_element(true, "description");
        showErrorMessage("Please put description ", 6);
        return;
    } else {
        error_input_element(false, "description");
    }
    loadingScreen(true, "Saving ..");
    $.post("app/ajax/ajaxes/add_aset.php",{
    	asstypeNew:234,
    	title:title,
    	description:description
    } ,response =>{
    	    loadingScreen(false, "Saving ..");
    	if(response === 'saved'){
		 	$("#title").val("");
		   $("#description").val('');
		   	showSuccessMessage("Saved .." , 4);
    	}else if (response === 'found'){
        showErrorMessage("Failed to save , Duplicate Found" , 4);
        }else{
    		showErrorMessage("Failed to save" , 4);
    	}

    });

}